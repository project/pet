<?php

namespace Drupal\pet\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Pet settings form class.
 *
 * @package Drupal\pet\Form
 * @ingroup pet
 */
class PetSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pet.settings';
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [
      'pet.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('pet.settings')
      ->set('pet_logging', $form_state->getValue('pet_logging'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $pet_logging = $this->config('pet.settings')->get('pet_logging');

    $form['logging'] = [
      '#type' => 'details',
      '#title' => $this->t('PET log settings'),
      '#open' => TRUE,
    ];

    $options = [
      0 => $this->t('Log everything.'),
      1 => $this->t('Log errors only.'),
      2 => $this->t('No logging, display error on screen, useful for debugging.'),
    ];

    $form['logging']['pet_logging'] = [
      '#type' => 'radios',
      '#title' => $this->t('Log setting'),
      '#options' => $options,
      '#default_value' => $pet_logging,
    ];

    return parent::buildForm($form, $form_state);
  }

}
