<?php

namespace Drupal\pet\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Pet form class.
 */
class PetForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['mimemail'] = [
      '#type' => 'details',
      '#title' => $this->t('Mime Mail options'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#open' => TRUE,
    ];
    $form['send_plain']['#group'] = 'mimemail';
    $form['mail_body_plain']['#group'] = 'mimemail';

    $form['mimemail']['#description'] = $this->t('HTML email support is most easily provided by the <a href="@url">Mime Mail</a> module, which must be installed and enabled.', ['@url' => 'http://drupal.org/project/mimemail']);
    // @todo Fix #2366853 mimemail integration
    if (!pet_has_mimemail()) {
      unset($form['mail_body_plain']);
      unset($form['send_plain']);
    }

    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Additional options'),
      '#open' => FALSE,
      '#access' => $this->currentUser()
        ->hasPermission('administer previewable email templates'),
    ];
    $form['cc_default']['#group'] = 'advanced';
    $form['bcc_default']['#group'] = 'advanced';
    $form['from_override']['#group'] = 'advanced';
    $form['recipient_callback']['#group'] = 'advanced';
    $form['actions']['submit']['#value'] = $this->t('Save Template');

    $form['tokens'] = pet_token_help();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $message_args = ['%label' => $this->entity->toLink()->toString()];
    $logger_args = [
      '%label' => $this->entity->label(),
      'link' => $this->entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New emailtemplate %label has been created.', $message_args));
        $this->logger('saf_example')->notice('New emailtemplate %label has been created.', $logger_args);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The emailtemplate %label has been updated.', $message_args));
        $this->logger('saf_example')->notice('The emailtemplate %label has been updated.', $logger_args);
        break;

      default:
        throw new \LogicException('Could not save the entity.');
    }

    $form_state->setRedirect('entity.pet.collection');
    return $result;
  }

}
