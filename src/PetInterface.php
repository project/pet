<?php

namespace Drupal\pet;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Pet entity interface.
 */
interface PetInterface extends ContentEntityInterface {

  /**
   * Returns the template title.
   *
   * @return string
   *   Title of the template.
   */
  public function getTitle();

  /**
   * Set the title for template.
   *
   * @param string $title
   *   Title of template.
   */
  public function setTitle($title);

  /**
   * Get status.
   *
   * @return int
   *   The status integer.
   */
  public function getStatus();

  /**
   * Set status.
   *
   * @param int $status
   *   The status integer.
   */
  public function setStatus($status);

  /**
   * Get subject.
   *
   * @return string
   *   The subject string.
   */
  public function getSubject();

  /**
   * Set subject.
   *
   * @param string $subject
   *   The subject string.
   */
  public function setSubject($subject);

  /**
   * Get mailbody.
   *
   * @return string
   *   The mailbody string.
   */
  public function getMailbody();

  /**
   * Set mailbody.
   *
   * @param string $mail_body
   *   The mailbody string.
   */
  public function setMailbody($mail_body);

  /**
   * Get mailbody plain.
   *
   * @return string
   *   The mailbody plain string.
   */
  public function getMailbodyPlain();

  /**
   * Set mailbody plain.
   *
   * @param string $mail_body_plain
   *   The mailbody plain string.
   */
  public function setMailbodyPlain($mail_body_plain);

  /**
   * Get send plain.
   *
   * @return mixed
   *   The send plain string.
   */
  public function getSendPlain();

  /**
   * Set send plain.
   *
   * @param string $send_plain
   *   The send plain string.
   */
  public function setSendPlain($send_plain);

  /**
   * Get recipient callback.
   *
   * @return mixed
   *   The recipient callback.
   */
  public function getRecipientCallback();

  /**
   * Set recipient callback.
   *
   * @param string $recipient_callback
   *   The recipient callback.
   */
  public function setRecipientCallback($recipient_callback);

  /**
   * Get cc default.
   *
   * @return mixed
   *   The cc default.
   */
  // phpcs:ignore
  public function getCCDefault();

  /**
   * Set cc default.
   *
   * @param $cc_default
   *   The cc default.
   */
  // phpcs:ignore
  public function setCCDefault($cc_default);

  /**
   * Get bcc default.
   *
   * @return mixed
   *   The bcc default.
   */
  // phpcs:ignore
  public function getBCCDefault();

  /**
   * Set bcc default.
   *
   * @param string $bcc_default
   *   The bcc default.
   */
  // phpcs:ignore
  public function setBCCDefault($bcc_default);

  /**
   * Get from override.
   *
   * @return mixed
   *   The from override.
   */
  public function getFromOverride();

  /**
   * Set from override.
   *
   * @param string $from_override
   *   The from override.
   */
  public function setFromOverride($from_override);

}
