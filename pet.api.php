<?php

/**
 * @file
 * PET API documentation.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Add custom token objects.
 *
 * Modules can implement this hook to provide additional token objects for
 * substitution by PET during an email send.
 */
function hook_pet_substitutions_alter(&$substitutions, $params) {
  // Make my tokens available to PET.
  if (isset($params['node']) && $params['node']->type == 'something_or_other') {
    $substitutions['something_or_other_extra_tokens'] = MY_MODULE_something_or_other_extra_tokens($params['node']);
  }
}

/**
 * Implements hook_default_ENTITY_TYPE().
 *
 * @see hook_default_pet_alter()
 */
function hook_default_pet() {
  $defaults['some_default_pet'] = \Drupal::service('entity_type.manager')
    ->getStorage('pet')
    ->create([
      'name' => 'some_default_pet',
      'title' => 'some default pet title',
      'subject' => 'subject',
      'mail_body' => 'body default',
      'from_override' => NULL,
      'cc_default' => 'cc@example.com',
      'bcc_default' => 'bcc@example.com',
      'recipient_callback' => 'MY_MODULE_recipients_callback',
    ]);
  return $defaults;
}

/**
 * Sample email recipient callback.
 *
 * In practice this would likely look up emails based on the node info.
 */
function hook_recipients_callback($node = NULL) {
  return [
    'allie@example.com',
    'bob@example.com',
  ];
}
